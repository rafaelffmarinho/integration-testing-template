# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


with open('README.rst') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='core',
    version='0.1.0',
    description='Integration Testing Package',
    long_description=readme,
    author='Rafael Marinho',
    author_email='rafael@joyjet.com',
    url='',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)
