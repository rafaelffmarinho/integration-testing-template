# content of conftest.py
import pytest

def pytest_addoption(parser):
    parser.addoption("--directory", action="store")
    parser.addoption("--server", action="store")