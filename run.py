import argparse
from core import path
import pytest


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--directory', type=path.fixpath)
    parser.add_argument('-s', '--server')
    pytest_args = parser.parse_args()
    #print('[%s]' % args.directory)
    #print('[%s]' % args.server)

    pytest.main(['--continue-on-collection-errors', '--directory', pytest_args.directory, '--server', pytest_args.server, 'tests/test_request.py'])