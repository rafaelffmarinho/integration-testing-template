# -*- coding: utf-8 -*-
import optparse
from .context import main, helpers, methods
import json
import logging as logger
import math
from nose.tools import assert_equal
from parameterized import parameterized
import pytest
import sys
import unittest


class AdvancedTestSuite(unittest.TestCase):
    
    """Advanced test cases."""
    def test_run_testcases(self):

        testcases_directory = pytest.config.getoption("--directory")
        test_server = pytest.config.getoption("--server")
      
        if not testcases_directory:
            print('A testcases directory must be supplied!')
            return

        if not test_server:
            print('A server to be tested must be supplied!')
            return

        logger.info('TestCase Directory ==> %s\n' % testcases_directory)
        logger.info('TestServer ==> %s\n' % test_server)

        logger.info("Finding testcases")
        file_list = helpers.get_test_files(testcases_directory, '.json')
        logger.info("Running testcases\n")

        for filename in file_list:
            expected_response = ""
            logger.info("----------------------------------------------------------------------------------------")
            logger.info("Processing {0}\n".format(filename))
            logger.info("----------------------------------------------------------------------------------------")

            with open("{0}/{1}".format(testcases_directory, filename), encoding="utf8") as data_file:
                expected_response = json.load(data_file)
                method = methods.find_method(expected_response["request"]["method"])
                
                actual_response = method(test_server,
                                         expected_response["request"]["additional_path"],
                                         expected_response["request"]["payload"])
                    
                logger.info("Expected Response\n{0}".format(expected_response["response"]))
                logger.info("Expected Response Code: {0}\n".format(expected_response["response_code"]))

                logger.info("Actual Response\n{0}".format(actual_response[0]))
                logger.info("Actual Response Code: {0}\n\n".format(actual_response[1]))
                assert_equal.__self__.maxDiff = None
                self.assertEqual(actual_response[0], expected_response["response"])
                self.assertEqual(actual_response[1], expected_response["response_code"])


if __name__ == '__main__':
    unittest.main()
