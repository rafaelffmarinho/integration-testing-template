import json
import logging as logger
import requests


def do_get(url, additional_path, payload):
    headers = {
        "Authorization": get_bearer_token(),
        "Content-Type": "application/json"    
    }
    complete_url = '{0}/{1}'.format(url, additional_path)
    logger.info("ENDPOINT: " + complete_url)
    raw_response = requests.get(complete_url, params=payload, headers=headers)

    return format_response(raw_response)


def do_post(url, additional_path, payload):
    headers = {"Authorization": get_bearer_token()}
    complete_url = '{0}/{1}'.format(url, additional_path)
    logger.info("ENDPOINT: " + complete_url)
    raw_response = requests.post(complete_url, data=payload, headers=headers)

    return format_response(raw_response)


def do_put(url, additional_path, payload):
    headers = {"Authorization": get_bearer_token()}
    complete_url = '{0}/{1}'.format(url, additional_path)
    logger.info("ENDPOINT: " + complete_url)
    raw_response = requests.put(complete_url, data=payload, headers=headers)

    return format_response(raw_response)


def not_implemented(url, additional_path, payload):
    return {}


def format_response(raw_response):
    response = json.loads(raw_response.text)
    response_code = raw_response.status_code
    
    return [response, response_code]


def find_method(method):
    if method in methods:
        return methods[method]
    else:
        logger.error("method {0} not implemented".format(method))
        return methods["not_implemented"]


def get_bearer_token():
    with open("authorization.config") as config_file:
        bearer = config_file.readline()

    return bearer


# map the inputs to the function blocks
methods = {
    "get": do_get,
    "post": do_post,
    "put": do_put,
    "not_implemented": not_implemented,
}


