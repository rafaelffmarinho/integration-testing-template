import os


## TODO need functionality to traverse a directory containing directories of json files
def get_test_files(directory, extension):
    file_list = []
    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.endswith(extension):
                file_list.append(file)
                print(os.path.join(root, file))
    return file_list
