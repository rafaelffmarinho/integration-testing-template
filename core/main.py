# -*- coding: utf-8 -*-
import logging


# TODO Setup log configuration in its own separate config file
# TODO dynamically determine name of log file
logging.basicConfig(filename='integration-testing.log', level=logging.INFO,
                    format='%(asctime)s - %(levelname)s - %(message)s')
logging.info("Further info in integration-testing.log'\n")
