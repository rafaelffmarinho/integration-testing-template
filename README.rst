Integration Test Repository
===========================

Requirements:

Python 3.6.1 (https://www.python.org/downloads/)

Initial Installation:
> pip3 install -r requirements.txt
> pip3 install pytest
> python setup.py install

To run the tests (examples)
> python run.py -d=tests\data -s=[API_URL]

E.g.

python run.py -d=tests\data -s=https://jsonplaceholder.typicode.com

Authorization:
> At the moment, it's done by simply adding your Bearer token to authorization.config
> [DO NOT COMMIT THE FILE WITH THE KEY]

Test Files:
> Test Files are created under tests/data as json files consisting of the request, the expected response and response code
> More examples under folder tests/examples

Errors:
> Errors are only displayed to the terminal when a test fails

Logs:
> Full Logs are written to integration-testing.log

Python style guide checker
https://pypi.python.org/pypi/pep8
